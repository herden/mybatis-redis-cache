package net.developerstation.mybatis.caches.redis.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import net.developerstation.mybatis.caches.redis.CacheException;
import de.ruedigermoeller.serialization.FSTObjectInput;
import de.ruedigermoeller.serialization.FSTObjectOutput;
public class SerializationUtils {

	public static byte[] serialize(Object obj) {
		return fstserialize(obj);
	}

	public static Object deserialize(byte[] bytes) {
		return fstdeserialize(bytes);
	}

	public static byte[] fstserialize(Object obj) {
		ByteArrayOutputStream out = null;
		FSTObjectOutput fout = null;
		try {
			out = new ByteArrayOutputStream();
			fout = new FSTObjectOutput(out);
			fout.writeObject(obj);
			fout.flush();
			return out.toByteArray();
		} catch (IOException e) {
			throw new CacheException(e);
		} finally {
			try {
				if (out != null)
					out.close();
				if (fout != null)
					fout.close();
			} catch (Exception e2) {
				throw new CacheException(e2);
			}
		}
	}

	public static Object fstdeserialize(byte[] bytes) {
		FSTObjectInput in = null;
		try {
			in = new FSTObjectInput(new ByteArrayInputStream(bytes));
			return in.readObject();
		} catch (Exception e) {
			throw new CacheException(e);
		} finally {
			try {
				if (in != null)
					in.close();
			} catch (Exception e2) {
				throw new CacheException(e2);
			}
		}
	}

	public static byte[] javaserialize(Object obj) {
		ObjectOutputStream oos = null;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			oos.writeObject(obj);
			return baos.toByteArray();
		} catch (IOException e) {
			throw new CacheException(e);
		} finally {
				try {
					if(oos!=null)
					oos.close();
				} catch (IOException e) {
					throw new CacheException(e);
				}
		}
	}

	public static Object javadeserialize(byte[] bits) {
		ObjectInputStream ois = null;
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(bits);
			ois = new ObjectInputStream(bais);
			return ois.readObject();
		} catch (Exception e) {
			throw new CacheException(e);
		} finally {
				try {
					if(ois!=null)
						ois.close();
				} catch (IOException e) {
					throw new CacheException(e);
				}
		}
	}

}
